I've decided to read the thread feedback and summarize the current state of Qt, and issues and ideas related to it.

- [QTBUG-63792 Bold fonts not hinted correctly](https://bugreports.qt.io/browse/QTBUG-63792)
- [QTBUG-65037 Need for more internal QFont consistency](https://bugreports.qt.io/browse/QTBUG-65037)
- [KFontRequester: Fonts are saved with face name preventing bold/italic from working](https://bugs.kde.org/show_bug.cgi?id=378523)
    - AFAIK this seems to be two or three separate problems under one thread: one where Konsole uses KFontRequester which adds styleName where there is none, possibly fixed by 2e971be576d24a82974eaf2397d4703fca0188d8, another fixed by removing ",styleName" from "~/.config/kdeglobals".

I've also commented with my own opinions and thoughts on the issue.

## The status quo

> QFont is a query for a font. The setStyleName() function was added to allow a user to override all other parts of the query with a specific style name.

> I don't want to make the order of property assignments on QFont have an impact on the results (nor do I think we would be able to do so for most of the properties on most platforms), so the requested properties of the QFont have to be evaluated in order of precedence, not in the order they were set. The style name property was always intended as the highest precedence when we added it in Qt 4.8. It was a solution for selecting fonts with styles which do not fit in the traditional PANOSE system and should only be used when you want to pick a specific style of a specific family.

Mental model: QFont is a query used to select a font; QFont is not a specific font. All properties are orthogonal and can be set independently. Setting styleName should not alter bold/weight/italic.

Reality: Apparently QFont does some caching. There's a bug where calling `QFont::fromString()` before setting a QFont to a widget causes the widget to reuse the prior styleName from before the call. The only way two compare-equal `QFont` can behave differently is with caching.

## Incomplete list of apps currently broken

In KDE Dolphin, symlinked files are faux italic. The fix is to remove ",Regular" from kdeglobals.

In KDE lock screen, the "Caps Lock is on" message is faux italic. (kdeglobals)

In KDE Okular, if you change what font the Markdown renderer uses (via QFontDialog), Okular sets styleName. This causes Markdown headings to use faux bold. The fix is to close Okular and remove ",Regular" from "~/.config/okular_markdown_generator_settings".

Apparently at one point, bold was broken in Konsole. AFAIK this is due to KFontRequester and not QFontDialog, and has been fixed.

In KDE Calligra Gemini (does anyone actually use Calligra?), even if you switch to a different font, you still have faux bold/italic. Probably due to kdeglobals. Could also be fixed if QFont's default constructor didn't inherit KDE's font settings (including styleName).

In Qt Creator, the monospace editor font has faux italic and no bold. "~/.config/QtProject/QtCreator.ini" only saves the font family, not entire `toString()`. So I think fixing the default QFont (kdeglobals) will fix this too.

On a related note, in corrscope (written by me), QFont is faux bold/italic. This is due due to a code bug in `QFont::fromString()` (QTBUG-80952) where I have already proposed a fix.

## Qt documentation on styleName

https://doc.qt.io/qt-5/qfont.html#setStyleName

> Due to the lower quality of artificially simulated styles, and the lack of full cross platform support, it is not recommended to use matching by style name together with matching by style properties

What about "MyLatinOnlyFont WeirdStyle"?

## Should KDE not write styleName to config files? Should QFontDialog not add them?

> > In my opinion, the style name should override all other properties when the selected style is available for the resolved font. The other properties should only take affect when selecting fonts that do not resolve to the style name.
> > [the style name] was a solution for selecting fonts with styles which do not fit in the traditional PANOSE system and should only be used when you want to pick a specific style of a specific family.
>
> Those are in direct contradiction, with the former making it impossible to for developers to follow the latter guideline except by implementing their own font dialog or patching Qt.

Conclusion: if Qt thinks style names should only be used for fonts which cannot be represented in PANOSE, then Qt's font dialog should only set styleName() for fonts which cannot be represented in PANOSE.

> The 2 main issues I'd address are the internal QFont inconsistency after calling setStyleName(), and the font dialog setting the stylename unconditionally. That internal state should always be as consistent as possible, including the style string (esp. that one as it comes from the font database). If consistency is not possible the font key and toString representations should drop the useless properties. (This could apply to fonts that cannot be represented in the PANOSE system?)

> A good fix would be in KFontRequester. It should clear the styleName after database lookup, if the same font is found when only using the family, weight and style attributes, possibly by doing two such lookups.
> I already noted how KFontRequester should be fixed: The styleName should be cleared, if the remaining attributes alone are sufficient to get the same face according to Qt's matching algorithms, which should work for nearly all font selections.

Seems reasonable. But if you pick a KDE global (family, font style) with a specific style name in to the font dialog, but later install more fonts in the same family, could the font KDE picks change unexpectedly?

> The impression I'm getting here is that the styleName property is something that's best left untouched except when you want "to match the font with irregular styles" (citing the 5.8.0 documentation for QFont). That documentation also suggests the properties in question might not be possible to capture with the other QFont attributes but I'd take that with a grain of salt. Qt determines that styleName somehow and presumably that's from metadata stored in the font and relayed through libfreetype of CoreText (styleName is apparently not used on other platforms, yet).

> https://codereview.qt-project.org/#/c/181645/
>
> QFont: fix fromString(toString()) when application font has styleName
>
> The style name needs to be cleared if not present in the string,
> otherwise the style name from qApp->font() (which propagates to
> any default-constructed QFont) remains.

## On KDE, empty QFont has attributes.

> > If you need to reset the query, make an empty QFont and only set the properties that you want to query for.
> That's one hell of an inelegant workaround for software that just tries to make things work like before as much as possible, making it mandatory to copy all desired attributes explicitly, rather than excluding a single one. (= a potential need for Qt version specific code if/when new attributes are introduced.)
> Even from the few tickets I've seen on bugs.kde.org there appear to be several KDE devs who consider this feature a regression and are looking for workarounds.
> EDIT: QFont may well be a query but in practice it's used throughout as the representation of a font. (Is it even likely that a given QFont instance gives a different typeface from reference to reference?)
> And turn around it as I may, I don't see why the fact QFont is a query or not makes any difference to `setStyleName` having an irreversible effect. (It kind of reminds me of the hype around prions ...)

> It seems that one reason why the style name is getting set has to do with the way font selections are saved in configuration files. They've started to include the stylename "extension" (11th or so parameter) and as several users have discovered, removing that bit manually resolves certain regressions they noticed. Some quick testing suggests that it's no longer safe to use QFont::toString() for saving font preferences.

On KDE, even empty `QFont()` has attributes equal to those of regular `QWidget::font()`. If kdeglobals contains ",Regular", so does empty `QFont()`. As a result, this workaround does *not* work; you need to `setStyleName("")` manually.

## From a typographical and internationalization perspective, is Qt's styleName good or bad? setBold/Italic?

> Your Superbold example is a case in point. Users of languages from certain "exotic" parts of the world may require the kind of compromise that setStyleName() currently implements. But I think that users of most languages in the western world (with the possible exception of Norwegian Troll dialects?) may want a different compromise that is more optimal for them. Exotic languagues/cultures may not even have the same tradition of using bold for instance, and then losing a reliable setBold() method isn't such a big deal for them.

Maybe allowing apps to bold/italicize fonts is inherently incompatible with non-Latin font families that don't follow that system.

> The styleName overrides other parameters. Imagine for instance you had asked for "Condensed" then we would be not be overriding that. Since styleNames can be anything including in any language, we can not know if it refers solely to a weight and unset it just because you are asking for a weight a different way.

Ouch. But this affects "Qt clears styleName when the app asks for bold" and "the app clears styleName before enabling bold" equally.

The Input programming font family makes every width of the font a separate family. As a result, bold/italic is easy to compute.

> So say that I have a font family called MyLatinOnlyFont with a non-standard bold style called WeirdStyle. The font only supports latin characters. I then do this:
> ```cpp
> QFont font("MyLatinOnlyFont");
> font.setStyleName("WeirdStyle");
> font.setBold(true);
> ```

Ouch. Maybe "Qt clears styleName when the app asks for bold" would break apps targeted around specific font families with known styles (apps where the user can't change which font family is used).

> From what I understand the use case where the issues appeared that I also encountered is simple: application code is designed to allow the user to select a font (say for the editor). This font is then used and its attributes are modified according to local requirements, like setting bold for headers or emphasised text. That sounds like exactly the kind of thing you'd use setBold() c.s. for...

- Some apps (Calligra) have bold/italic toggles for rich text fields; you could argue this is an antipattern and apps should use a font picker instead, but Ctrl+B/I is convenient, and bold/italic is well-established in HTML/Word.
- Some programs (Dolphin, Okular markdown rendering) use bold/italic in programmatically generated output to convey semantic information. Bold/weight and italic is a useful set of parameters (and natural, at least for Latin text) for programs to specify, and Qt to pick, distinguishable font styles for different text. Alternatively the program could be written for a specific font family and directly use the styles supplied by it, but may not function well with other fonts.
- Most code editors and IDEs (Qt Creator broken, Konsole works due to workaround) support syntax highlighting themes which bold/italicize text dynamically. I expect the theme to be configurable and work for any font family I pick with a bold and italic, even more strongly than I expect apps to be configurable. At least from what I've heard, some programmers (including myself) have strong font preferences, which differ between people.
    - On the other hand, I have seen people using different font families for different type of syntax, for example italic alongside regular. This is uncommon, possibly because it's hard to achieve on the conventional bold/italic-toggle system. "Which font is used for regular text? italic text?" could be stored in each theme, globally in the IDE and kept if the user switches themes, or in global @font-face metafonts pointing to real fonts.

While I believe font pickers can solve some use cases, I still expect boolean bold/italic switches to pick the right fonts out of font families, if they supply multiple weights/styles which can be made to fit into a bold/italic system. For other fonts, you could either do nothing or use faux-bold or faux-italic. I've seen Chinese fonts (like Source Han Sans/Serif) which support various weights (they work well, at least on paper and high DPI screens) but generally not italic; marking them italic anyway results in ugly faux italic. Also, faux bold/italic emojis are horrifying.


# Writing a test program

I have done some further investigation at https://gitlab.com/nyanpasu64/qt-italic-bug-demo/tree/qfont-style (tag: qfont-style). The main program is at https://gitlab.com/nyanpasu64/qt-italic-bug-demo/blob/qfont-style/main.cpp , and you can swap `if(false)` and `if(true)` to change program behavior.

Qt has several themes or font sets.

- `QKdeTheme` is used on KDE. The default QFont and QWidget font depends on your KDE font settings.
- `QGenericUnixTheme` is used on DEs other than GNOME (possibly including XFCE). The default QFont and QWidget font is not configurable(?) For me, it's "Sans Serif" which looks like Bitstream Vera or DejaVu Sans.

To trigger the non-KDE code path (switch to `QGenericUnixTheme`) and avoid this bug, alter your shell environment as follows:

```
set -e XDG_CURRENT_DESKTOP
set -e KDE_FULL_SESSION
set -e DESKTOP_SESSION
```

## Results

### Behaviors consistent with prior messages in this thread

On KDE, the fonts specified in "~/.config/kdeglobals" are used for the default QWidget `w.font()`. Since KDE's font picker writes a styleName to kdeglobals, the default QWidget.font() contains a non-empty `styleName()`. `QFont::styleName()` overrides subsequent setBold/setItalic, when looking up which font file to use.

### Behaviors inconsistent with prior messages in this thread

Even empty QFont() has attributes equal to those of regular QWidget::font(). If kdeglobals contains ",Regular", so does empty QFont().

----

On KDE, after you call w.setFont(f), w's appearance is solely determined by f, with no properties inherited from w.parent().font().

- If I call `widget.setFont(QFont{})`, `widget` no longer inherits bold=true or italic=true or styleName="Bold Italic" from its parent.
- Maybe this is because on KDE, `QFont()` default constructor depends on QKdeTheme::font(), which returns a font which already defines all attributes. (I haven't tested on other platforms.)

----

On KDE, if I remove `,Regular` from kdeglobals, default-constructed QFont (unset styleName) behaves identically to fonts with setStyleName(""). If you `w.setFont(f)`, both `f` override a non-empty `styleName` of `w.parent().font()`.

Maybe when default-constructing a QFont, `QKdeTheme` (part of Qt, not KDE) sets styleName="" even if you remove `,Regular` from kdeglobals. Unfortunately QFont's public API has no way to distinguish unset and true/false italic, and unset vs empty styleName. QTBUG-65037 claims that they function differently, but I have observed no such differences.

`QWidget::setFont(QFont with unset styleName)` can inherit styleName from the parent widget. It calls `QFont::resolve` which calls `QFontPrivate::resolve` which uses `QFont::resolve_mask` to know whether styleName() is empty (use it) or unset (inherit from the parent).

By casting a QFont* to my own all-public copy of QFont, I verified that `resolve_mask & StyleNameResolved` is set even on an default-constructed QFont on KDE. Specifically, resolve_mask is 0x10173 (StyleNameResolved=0x10000 is set), even when kdeglobals doesn't contain a final styleName parameter! Is this intentional or a bug?

----

How do fonts (QFont::toString() values) stored in kdeglobals make its way to the default QFont?

- QFont::QFont() defaults to QGuiApplication::font().
- QGuiApplication::font() initializes QGuiApplicationPrivate::app_font via initFontUnlocked(), before returning app_font.
- initFontUnlocked() sets QGuiApplicationPrivate::app_font to (QKdeTheme extends QPlatformTheme)::font(QPlatformTheme::SystemFont).
- QKdeTheme::font() returns QKdeThemePrivate.resources.fonts[...].
- QKdeThemePrivate.resources.fonts is initialized when QKdeThemePrivate::refresh() calls QKdeThemePrivate::kdeFont() on QFont::toString() values stored in kdeglobals.
- QKdeThemePrivate::kdeFont uses QFont::QFont() and fromString().
- Prior to QGuiApplicationPrivate::instance() being initialized, QFont::QFont() defaults to QFontPrivate::QFontPrivate().

----

f.toString() produces the same result (no styleName parameter) even after you setStyleName(""). Apparently, QFont::toString() omits styleName whenever it's empty, even if styleName was explicitly set (`resolve_mask & StyleNameResolved` is set)!


# Proposed changes to KDE and Qt

My opinion: At the moment, removing styleName is necessary to fix setBold/Italic in most Qt apps. It would decrease compatibility with fonts that don't fit into PANOSE, and increase the chance that installing new fonts will change how the desktop looks.

To me, the most elegant solution is that styleName would be saved, so any arbitrary system font could be saved and loaded losslessly. However, styleName is an opaque identifier, so it is impossible for Qt to automatically identify the bold/italic version of a styleName when a program wants it. The best we can do is to take the QFont, alter the italic/bold parameters, and erase styleName so Qt tries to pick an appropriate font. (In practice, there are many apps out there that call setBold without setStyleName(""). I don't think I can change them all.) Later I propose an API `setAttributes()` to enforce that apps erase/rewrite styleName whenever they set bold/italic.

Is there a way to take a single font like "DejaVu Sans" "Semi Condensed", enable bold and italic, and Qt will determine the styleName you want is "Bold Semi Condensed Oblique"? I've gotten the impression that this is either not possible, or not within the scope of QFont (which is a a database query, not the results of a lookup). Is it possible if you perform a database query?

## Default-constructed QFont's resolve_mask should be completely unset, and "resolve_mask set" should override "resolve_mask unset"

Qt's intent is that QFont is a tri-state system where bold is either true, false, or unset (`resolve_mask & WeightResolved` is unset), and styleName is either a string or unset (`resolve_mask & StyleNameResolved` is unset). (Note: QFont::setBold() is merely a wrapper for setWeight, and setItalic is a wrapper for setStyle. They're not independent axes.)

Right now, default-constructed QFont contains default values (font name, size, weight, style, styleName) out of the box. Suppose we leave this API as-is.

Proposed changes to Qt behavior:

- Modify KDE Qt's default QFont, so it comes with an all-zeros `resolve_mask`.
- Calling setBold(bool) would set `resolve_mask & WeightResolved`. Calling setItalic(bool) would set `resolve_mask & StyleResolved`
- If a font's `WeightResolved` or `StyleResolved` is set but not `StyleNameResolved`, then when a widget uses this font, the style name is treated as empty, and both this font's `styleName()` and the widget's inherited font 's `styleName()` are ignored.
    - When you call `QWidget::setFont(font)`, `resolvedFont.styleName()` (merged font) will be "". Both `font.styleName()` (input font) and `naturalFont.styleName()` (QWidget's parent font) are ignored.

Advantages:

- Fixes many existing apps, and makes setBold/Italic work intuitively. Even if styleName is set by default, setBold still works.
- setBold and setStyleName can be called in any order with the same results. So "WeirdStyle" will work like it does now.
- Self-consistent and logical to me.

Disadvantages:

- Does it change the mental model of QFont?
- Does changing `QFont()`'s value on KDE break backwards compatibility with Qt's previous KDE default fonts?
    - I think the resulting behavior will be closer to Windows, where the default QFont has either empty or unset styleName (unsure which), and fonts behave sanely (no faux bold/italic).
- Does changing `QWidget::setFont()`'s behavior break backwards compatibility with apps?
- Does changing `QFont::private resolve()`'s return break Qt-internal callers other than `QWidget::setFont()`?
- Doesn't fix apps which call `QFont::fromString()` or use `QFontDialog`. See below "priority levels".

Might as well throw in a `void unsetFont()` and `bool isFontSet()` API along the way.

## QFont attribute priority levels

Given the above API, if an app obtains a QFont directly from QFontDialog or fromString(), the app will still use faux bold/italic unless the app (not Qt) first clears styleName() when calling setBold or setItalic. My above API only fixes QFont where styleName is set by KDE's font settings.

All apps which call QFontDialog, or save the obtained font to disk and load it via fromString(), are affected. Examples include Okular and ReText.

Maybe QFont can store a numeric/enum "priority level" for each attribute? "default value of QFont()" = 0, "set via fromString or QFontDialog" = 1, or "overriden via setBold/Italic/styleName()" = 2. Only positive-priority (non-default) attributes override styles inherited from parent QWidgets. And if `bold` has higher priority than `styleName`, `styleName` is ignored when calling `QWidget::setFont()`.

If an app loads a font from QFontDialog or string on disk, styleName has priority 1. If it then enables bold, weight has priority 2 and disables styleName. This ensures the app doesn't use a font with faux bold.

Disadvantages:

- Complicated API, may be difficult to learn?
- May require extra data to be stored in QFontPrivate, which overlaps in functionality with `resolve_mask`. `resolve_mask` is 32 bits, but has over half its bits allocated already.
- (I thought I invented an elegant new API to solve this issue once and for all... but I instead introduced a whole new layer of complexity.)

Instead of `bool isFontSet()`, add a `{int or enum} fontPriority()` method or something.

## (OLD) Default-constructed QFont should be empty, not have platform-specific settings

Right now, default-constructed `QFont()` has a defined family, size, bold/italic, and (later added) styleName. An app which constructs a font from scratch needs to set every single one of these attributes before using the font. Calligra (KDE office program) forgets to set styleName, and consequently only ever uses Regular-styled fonts and applies faux bold onto them.

The default-constructed `QFont` should be a clean slate. QWidget's default QFont should obtain its attributes through `QPlatformTheme::font(...)`, which will have attributes not found in `QFont()`.

Unfortunately this change will be backwards-incompatible and break apps which expect `QFont()` to produce the same font used in QWidget by default.

Why would an app do that? Maybe it wants to italicize a widget.

- setStyleSheet(...)
- QFont f{w.font()}; f.setItalic(true); w.setFont(f);
- QFont f; f.setItalic(true); w.setFont(f);

Currently, the last two lines of code do the same thing if `w` starts out with the default widget font (not inheriting an altered font). My proposed change would break the last setup.

## (OLD) setBold/setItalic clears styleName?

One idea I had is that setBold/setItalic would clear styleName. If you want to set both bold and styleName, you can call setStyleName() after setBold(), or use setAttributes() instead. Maybe setBold/Italic/StyleName could even be deprecated, though this may be inconvenient and unpopular.

Advantages:

- Fixes many existing apps, and makes setBold/Italic work intuitively
- Mimics the behavior of old Qt, where styleName didn't exist and was not set by QFontDialog

Disadvantages:

- Reduces the conceptual elegance of QFont
- Results in inconsistent behavior. Reordering setBold and setStyleName changes the resulting QFont.

## (OLD) API: `setAttributes(QFont&, bool? bold, int? weight, bool? italic, string styleName)`

Prior to writing my first proposal ("resolve_mask set" should override "resolve_mask unset"), I created and implemented another API. `setAttributes()` has the advantage of not breaking backwards compatibility at all, but the disadvantage of leaving behind an unintuitive API, leaving existing apps broken, as well as being a bit verbose and having too many positional parameters. Both APIs would ensure that enabling bold/italic wouldn't get overriden by a pre-existing styleName.

`QFont::setAttributes(bold=true|false|unset, weight=int|unset, italic=true|false|unset, styleName=str|none)`. Most parameters are inherited from a QWidget's parent if unset. `styleName` overrides the other parameters if it's not none, and otherwise gets unset (not inherited from parent).

- If called from a function which bolds/italicizes any arbitrary QFont(family, style name) passed in (like a syntax highlighting theme), `styleName` would be `none`. Qt would assume each font family only varies by weight and italic (eg. condensed would be a separate font family). In this case, Qt could later look up the exact font requested, without guesswork.
    - A `@font-face`-like mechanism would extend this method to work with unusual fonts as well.
- For font selection based around the capabilities of a specific font, styleName would be set (eg. how `QFontDialog` does it), and the other parameters would serve as fallback only.
- There is no option to unset `styleName` (inherit it from parent widget), because if set, it's the sole determinant of both the old and new QFont's output. Therefore the function call would have no visible effect (except for telling Qt how to pick a fallback font).

I think this can already be approximately implemented:

- May not work perfectly if QFont defaults to bold, your parent QWidget is unbold, and you expect your inner QWidget to be unbold by unset.
- Passing in Noop will preserve current value, not inherit from parent. Maybe this is a feature.
- Both of these issues result because default-constructed QFont has both state and `resolve_mask` set by default.

I have a working implementation of `setAttributes()` (a free function, not a method) posted at https://gitlab.com/nyanpasu64/qt-italic-bug-demo/blob/qfont-style/qfont_util.h . It's already being used in the demo program in that repo.

```cpp
/// bold/weight/italic will *optionally* modify font.
/// styleName will unconditionally be written to font.
static void setAttributes(
    QFont & font,
    MaybeBool bold,
    MaybeUnsigned weight,
    MaybeBool italic,
    MaybeString const & styleName
) {
    if (is_set(bold)) font.setBold(value(bold));
    if (is_set(weight)) font.setWeight(value(weight));
    if (is_set(italic)) font.setItalic(value(italic));
    font.setStyleName(styleName);
}
```

### Use cases of `setAttributes()`

> My approach here (which seems to match that used in Gnome/GTk and CoreText) is: does the font have an intermediate weight (63 or so); use that if found, otherwise use the next weight up because we want something visibly bolder than regular.

Does GTK rely on querying the font database when "relatively emboldening" a font?

it's simple to go from `QFont(weight=QFont::Bold, styleName=unset)` to `QFont(weight=QFont::ExtraBold, styleName=unset)`. This could be implemented in an moreBold() function which wraps `QFont::setAttributes()`. Firefox's `<b>` tag does this, which is standards-compliant. Chrome's `<b>` tag doesn't make bold any heavier, and some websites rely on this nonstandard behavior and look wrong on Firefox.

```cpp
// TODO use QFontDatabase.
// Because QFontInfo thinks different QFont have different weights,
// even if they resolve to the same .ttf/otf.
static void moreBold(QFont & font) {
    int bold_weight = [&]() {
        int w = QFontInfo{font}.weight();

        // 0 => 25
        if (w <= QFont::Thin) return QFont::Light;
        // 25 => 50
        if (w <= QFont::Light) return QFont::Normal;
        // 50 => 75
        if (w <= QFont::Normal) return QFont::Bold;
        // 57 => 81
        if (w <= QFont::Medium) return QFont::ExtraBold;
        // 63 => 87
        if (w <= QFont::DemiBold) return QFont::Black;
        // 99 => 87
        return QFont::Black;
        // yeah i know the last two branches are the same.
    }();
    setAttributes(font, MaybeBool{}, MaybeUnsigned {bold_weight}, MaybeBool{}, "");
}
```

apparently qt has no way to query whether a font actually exists or not. QFontInfo thinks bold 0/1/2 times have different thicknesses, even on fonts with nothing below 50. How does QFontDialog know?

If you start with `QFont(weight=QFont::Light, styleName="Bold")` (a very confused font query, hopefully setAttributes() will discourage people from doing this), emboldening it to `QFont(weight=QFont::ExtraBold, styleName=unset)` (which requires querying to font database to find the weight of "Bold") or `weight=QFont::Normal` (which does not require a database query) is not as obviously correct. And if it were "Bold Condensed", there's no way but guesswork to know the resulting font needs to be Condensed as well.

Going from `QFont(weight=unset, styleName="Bold")` to `QFont(weight=QFont::ExtraBold, styleName=???)` would be impossible unless you look up the QFont before you even use it in a widget. Which seems to be incompatible with the concept that QFont is merely a query.

## @font-face

I also think a @font-face-like API would be useful, but I have no concrete suggestions yet. Some fonts have unusual styleName variants which Qt doesn't know how to bold/italicize. Apps could instead define a synthetic font `fake family`, and define their own mapping from `(fake family, weight, italic, maybe fake styleName)` to `(real family, real styleName)` (the unusual font).

To the best of my knowledge, CSS allows you to define a web `@font-face` family named "MyLatinOnlyFont", with a WeirdStyle pointing to either a URL or a local font called "MyLatinOnlyFont WeirdStyle" (unsure about priorities), and WeirdStyle marked as bold. If you mark WeirdStyle as bold, using that MyLatinOnlyFont in a bold element (like a heading) will pick WeirdStyle as-is, and not faux-bold the already-bold font. I have no clue if this can be adapted to Qt.

https://developer.mozilla.org/en-US/docs/Web/CSS/@font-face

```css
@font-face {
  font-family: MyHelvetica;
  src: local("Helvetica Neue Bold"),
       local("HelveticaNeue-Bold"),
       url(MgOpenModernaBold.ttf);
  font-weight: bold;
}
```

But in the case of a code editor, I would probably be fine if the app had code to "clear styleName before enabling bold", and didn't have a way to use WeirdStyle.
